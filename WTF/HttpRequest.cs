﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Net;
using System.Drawing;

namespace WTF
{
    public static class HttpRequest
    {
        public static CookieCollection cookies = new CookieCollection();

        #region 获取验证码
        public static Stream getCode(string url)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.ContentType = "application/x-www-form-urlencoded; charset=GBK";
            request.Method = "GET";
            request.UserAgent = "Mozilla/4.0";
            request.CookieContainer = new CookieContainer();
            request.CookieContainer.Add(cookies);

            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            cookies = response.Cookies;
            Stream responseStream = response.GetResponseStream();
            return responseStream;
        }
        #endregion

        #region 投票开始
        public static string doPost(string code)
        {
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create("http://www.findbest.cn/wp-content/themes/zuimeicounty/countrysupport/country-support.php?supportid=10531&code=" + code);
                //打包Post数据
                request.ContentType = "application/x-www-form-urlencoded; charset=GBK";
                request.Method = "POST";
                request.UserAgent = "Mozilla/4.0";
                request.CookieContainer = new CookieContainer();
                request.CookieContainer.Add(cookies);
                byte[] Poststh = Encoding.Default.GetBytes("supportid=10531&code=" + code);
                request.ContentLength = Poststh.Length;
                //获得请求流
                Stream requestStream = request.GetRequestStream();
                //将请求参数写入流
                requestStream.Write(Poststh, 0, Poststh.Length);
                // 关闭请求流
                requestStream.Close();

                HttpWebResponse response = (HttpWebResponse)request.GetResponse();

                Stream responseStream = response.GetResponseStream();
                StreamReader responseReader = new StreamReader(responseStream, Encoding.Default);
                string result = responseReader.ReadToEnd();

                response.Close();
                responseStream.Close();
                responseReader.Close();

                return result;
            }
            catch (Exception ex)
            {
                return "err" + ex.Message;
            }
        }
        #endregion

    }
}
