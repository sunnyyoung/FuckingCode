﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Imaging;
using System.Text;
using System.Windows.Forms;
using tesseract;

namespace WTF
{
    public partial class MainForm : Form
    {
        tesseract.TesseractProcessor ocr;
        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            ocr = new tesseract.TesseractProcessor();
            bool succeed = ocr.Init(@"..\..\tessdata\", "eng", 3);
            if (!succeed)
            {
                MessageBox.Show("Tesseract initialization failed. The application will exit.");
                Application.Exit();
            }
            ocr.SetVariable("tessedit_char_whitelist", "0123456789abcdefghijklmnopqrstuvwxyz");
        }

        private void codeBox_Click(object sender, EventArgs e)
        {
            refreshCode();
        }

        //验证码识别返回字符串
        private string Ocr(Image image)
        {
            ocr.Clear();
            ocr.ClearAdaptiveClassifier();
            return ocr.Apply(image);
        }
        private void btnStart_Click(object sender, EventArgs e)
        {
            Timer.Enabled = true;
        }

        public void refreshCode(int times = 1)
        {
            Bitmap code = new Bitmap(HttpRequest.getCode("http://www.findbest.cn/wp-content/themes/zuimeicounty/countrysupport/code_num_9497.php?cid=10531&time=0.28996047679018205"));
            code = PicFix.GrayByPixels(code);
            //code = PicFix.PBinary(code, 128);
            code = PicFix.ClearNoise(code, PicFix.GetDgGrayValue(code), 1);
            codeBox.Image = code;
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            
        }

        private void resultTextBox_TextChanged(object sender, EventArgs e)
        {
            
        }
    }
}
